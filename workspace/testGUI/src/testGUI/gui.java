package testGUI;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class gui extends JFrame {
    private static final long serialVersionUID = 1L;

    String qImG = "";
    public gui() {
    	 setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
         setBounds(100, 100, 1285, 642);

         JPanel contentPane = new JPanel();
         contentPane.setPreferredSize(new Dimension(1280, 640));
         contentPane.setBackground(Color.lightGray);
         contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
         setContentPane(contentPane);
         contentPane.setLayout(null);

         JLabel ISELbl = new JLabel("Image Search Engine");
         ISELbl.setFont(new Font("Calibri", Font.BOLD, 16));
         ISELbl.setHorizontalAlignment(SwingConstants.CENTER);
         ISELbl.setBounds(63, 30, 171, 24);
         contentPane.add(ISELbl);
             
         JLabel resultsLbl = new JLabel("Results");
         resultsLbl.setFont(new Font("Calibri", Font.BOLD, 16));
         resultsLbl.setHorizontalAlignment(SwingConstants.CENTER);
         resultsLbl.setBounds(855, 30, 100, 24);
         contentPane.add(resultsLbl);

         Border loweredbevel = BorderFactory.createLoweredBevelBorder();
        
         JButton btnUpdate = new JButton("Update Database");
         btnUpdate.setBounds(63, 70, 171, 24);
         contentPane.add(btnUpdate);
         btnUpdate.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
			}
  
         });

         JButton btnBrowse = new JButton("Browse...");
         btnBrowse.setBounds(63, 100, 171, 24);
         contentPane.add(btnBrowse);
         btnBrowse.addActionListener(new ActionListener() {
             public void actionPerformed(ActionEvent arg0) {
                 
             }
         });
         
         JLabel lblSearchImg = new JLabel("pending...");
         lblSearchImg.setBackground(Color.white);
         lblSearchImg.setHorizontalAlignment(SwingConstants.CENTER);
         lblSearchImg.setBounds(100, 200, 256, 256);
         
         getContentPane().add(lblSearchImg);
         lblSearchImg.setBorder(loweredbevel);
         
        
        JLabel lblResImg_1 = new JLabel("Image 1");
        lblResImg_1.setHorizontalAlignment(SwingConstants.CENTER);
        //lblResImg_1.setBounds(100, 200, 80, 20);
        //getContentPane().add(lblResImg_1);
        lblResImg_1.setBorder(loweredbevel);
        
        JLabel lblResImg_2 = new JLabel("Image 2");
        lblResImg_2.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblResImg_2);
        
        JLabel lblResImg_3 = new JLabel("Image 3");
        lblResImg_3.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblResImg_3);
        
        JLabel lblResImg_4 = new JLabel("Image 4");
        lblResImg_4.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblResImg_4);
        
        JLabel lblResImg_5 = new JLabel("Image 5");
        lblResImg_5.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblResImg_5);
        
        JLabel lblResImg_6 = new JLabel("Image 6");
        lblResImg_6.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblResImg_6);
        
        JLabel lblResImg_7 = new JLabel("Image 7");
        lblResImg_7.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblResImg_7);
        
        JLabel lblResImg_8 = new JLabel("Image 8");
        lblResImg_8.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblResImg_8);
        
        JLabel lblResImg_9 = new JLabel("Image 9");
        lblResImg_9.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblResImg_9);
        
        JLabel lblResImg_10 = new JLabel("Image 10");
        lblResImg_10.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblResImg_10);
        
        JLabel lblResImg_11 = new JLabel("Image 11");
        lblResImg_11.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblResImg_11);
        
        JLabel lblResImg_12 = new JLabel("Image 12");
        lblResImg_12.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblResImg_12);
        
        JLabel lblResImg_13 = new JLabel("Image 13");
        lblResImg_13.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblResImg_13);
        
        JLabel lblResImg_14 = new JLabel("Image 14");
        lblResImg_14.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblResImg_14);
        
        JLabel lblResImg_15 = new JLabel("Image 15");
        lblResImg_15.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblResImg_15);
        
        JLabel lblQImg = new JLabel();
        lblQImg.setHorizontalAlignment(SwingConstants.CENTER);
        getContentPane().add(lblQImg);
        
        JButton btnSearch = new JButton();
        btnSearch.setText("Search");
        getContentPane().add(btnSearch);
        
        JButton btnClear = new JButton();
        btnClear.setText("Clear data");
        getContentPane().add(btnClear);
        
        JLabel label = new JLabel("");
        getContentPane().add(label);

        setVisible(true);
    }

    public static void main(String[] args) {
        gui myGUI = new gui();
    }
}